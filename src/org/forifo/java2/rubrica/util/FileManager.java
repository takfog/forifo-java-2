package org.forifo.java2.rubrica.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.forifo.java2.rubrica.contatti.Azienda;
import org.forifo.java2.rubrica.contatti.Contatto;
import org.forifo.java2.rubrica.contatti.Persona;
import org.forifo.java2.rubrica.recapiti.Email;
import org.forifo.java2.rubrica.recapiti.Indirizzo;
import org.forifo.java2.rubrica.recapiti.IndirizzoItaliano;
import org.forifo.java2.rubrica.recapiti.IndirizzoStraniero;
import org.forifo.java2.rubrica.recapiti.Recapito;
import org.forifo.java2.rubrica.recapiti.Telefono;

public class FileManager {
	
	public static ContenutoFile carica(String filename) {
		File file = new File(filename);
		if (!file.isFile())
			throw new RuntimeException("Il file "+file.getAbsolutePath()+" non esiste");
		
		Map<String, Persona> persone = new HashMap<>();
		Map<String, Azienda> aziende = new HashMap<>();
		Map<String, String> dipendenze = new HashMap<>();
		try (BufferedReader r = new BufferedReader(new FileReader(file))) {
			for(String l = r.readLine(); l != null; l = r.readLine()) {
				Contatto c = deserializza(l,dipendenze);
				if (Persona.class.equals(c.getClass()))
					persone.put(c.getId(), (Persona) c);
				else
					aziende.put(c.getId(), (Azienda) c);
			}
		} catch (IOException e) {
			throw new UncheckedIOException("Errore durante la lettura di "+file.getAbsolutePath(), e);
		}
		
		for (Entry<String, String> e : dipendenze.entrySet()) {
			if (persone.containsKey(e.getKey())) {
				Persona p = persone.get(e.getKey());
				Azienda a = aziende.get(e.getValue());
				if (a == null)
					throw new RuntimeException("Impossibile trovare l'azienda "+e.getValue()
					+" per cui lavora "+e.getKey());
				p.setLavoro(a);
			} else {
				Azienda a = aziende.get(e.getKey());
				Persona p = persone.get(e.getValue());
				if (p == null)
					throw new RuntimeException("Impossibile trovare la persona "+e.getValue()
					+" direttore di "+e.getKey());
				a.setDirettore(p);
			}
		}
	
		rimuoviNonIn(persone);
		rimuoviNonIn(aziende);
		
		Persona[] parr = persone.values().toArray(new Persona[persone.size()]);
		Azienda[] aarr = aziende.values().toArray(new Azienda[aziende.size()]);
		return new ContenutoFile(parr, aarr);
	}

	private static void rimuoviNonIn(Map<String, ? extends Contatto> map) {
		ArrayList<Contatto> cont = new ArrayList<>(map.values());
		for (Contatto c : cont) {
			if (c.getRecapiti() != null) {
				boolean nonIn = false;
				for (Recapito r : c.getRecapiti()) {
					if (NonInRubrica.class.equals(r.getClass())) {
						nonIn = true;
						break;
					}
				}
				if (nonIn) {
					map.remove(c.getId());
					ArrayList<Recapito> rec = new ArrayList<>(c.getRecapiti().length-1);
					for (Recapito r : c.getRecapiti()) {
						if (!NonInRubrica.class.equals(r.getClass()))
							rec.add(r);
					}
					c.setRecapiti(rec.toArray(new Recapito[rec.size()]));
				}
			}
		}
	}
	
	public static void salva(String filename, Persona[] persone, Azienda[] aziende) {
		Set<String> ids = new HashSet<>();
		ArrayList<Contatto> nonIn = new ArrayList<>();
		for (Contatto c : persone) {
			ids.add(c.getId());
		}
		for (Contatto c : aziende) {
			ids.add(c.getId());
		}
		for (Persona p : persone) {
			Azienda lavoro = p.getLavoro();
			if (lavoro != null && !ids.contains(lavoro.getId()))
				nonIn.add(lavoro);
		}
		for (Azienda a : aziende) {
			Persona direttore = a.getDirettore();
			if (direttore != null && !ids.contains(direttore.getId()))
				nonIn.add(direttore);
		}
		
		File file = new File(filename).getAbsoluteFile();
		file.getParentFile().mkdirs();
		
		try (PrintWriter w = new PrintWriter(file)) {
			for (Persona p : persone) {
				serializza(w, p, true);
			}
			for (Azienda a : aziende) {
				serializza(w, a, true);
			}
			for (Contatto c : nonIn) {
				if (Persona.class.equals(c.getClass()))
					serializza(w, (Persona)c, false);
				else
					serializza(w, (Azienda)c, false);
			}
		} catch (IOException e) {
			throw new UncheckedIOException("Errore durante la scrittura del file "+file.getAbsolutePath(), e);
		}
	}

	private static void serializza(PrintWriter w, Azienda a, boolean inRubrica) {
		if (inRubrica)
			w.print("A");
		else
			w.print("L");
		w.print("\t"+a.getId()+"\t"+a.getNome()+"\t");
		if (a.getDirettore() != null)
			w.print(a.getDirettore().getId());
		serializzaRecapiti(w, a);
		w.println();
	}

	private static void serializza(PrintWriter w, Persona p, boolean inRubrica) {
		if (inRubrica)
			w.print("P");
		else
			w.print("D");
		w.print("\t"+p.getId()+"\t"+p.getCognome()+"\t"+p.getNome()+"\t");
		if (p.getNascita() != null)
			w.print(p.getNascita().getTimeInMillis());
		w.print("\t");
		if (p.getLavoro() != null)
			w.print(p.getLavoro().getId());
		serializzaRecapiti(w, p);
		w.println();
		
	}
	
	private static void serializzaRecapiti(PrintWriter w, Contatto c) {
		if (c.getRecapiti() == null)
			return;
		for (Recapito r : c.getRecapiti()) {
			char cod;
			Class<? extends Recapito> clazz = r.getClass();
			if (Telefono.class.equals(clazz)) 
				cod = 'T';
			else if (Email.class.equals(clazz))
				cod = 'E';
			else if (IndirizzoItaliano.class.equals(clazz))
				cod = 'I';
			else if (IndirizzoStraniero.class.equals(clazz))
				cod = 'S';
			else
				continue;
			
			w.print("\t"+cod+"\t");
			
			switch (cod) {
			case 'T':
			case 'E':
				w.print(r.getDescrizione()+"\t"+r.getValore());
				break;
			case 'I':
			case 'S':
				Indirizzo i = (Indirizzo)r;
				w.print(r.getDescrizione()+"\t"+i.getVia()+"\t"+i.getCitta()+"\t");
				if (cod=='S')
					w.print(i.getStato());
				else {
					IndirizzoItaliano ii = (IndirizzoItaliano)i;
					w.print(ii.getProvincia()+"\t"+ii.getCap());
				}
			}
		}
	}
	
	private static Contatto deserializza(String line, Map<String,String> dipendenze) {
		String[] parts = line.split("\t");
		int i = 0;
		boolean inRubrica = true;
		Contatto c;
		String id = parts[1];
		switch (parts[0]) {
		case "D":
			inRubrica = false;
		case "P":
			Persona p = new Persona(id, parts[2], parts[3]);
			c = p;
			if (parts.length > 4 && !parts[4].isEmpty()) {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTimeInMillis(Long.parseLong(parts[4]));
				p.setNascita(cal);
			}
			i = 5;
			break;
		case "L":
			inRubrica = false;
		case "A":
			c = new Azienda(id, parts[2]);
			i = 3;
			break;
		default:
			return null;
		}
		
		if (parts.length > i && !parts[i].isEmpty())
			dipendenze.put(id, parts[i]);
		i++;
		
		//recapiti
		ArrayList<Recapito> recapiti = new ArrayList<>();
		if (!inRubrica)
			recapiti.add(new NonInRubrica());
		while (i < parts.length) {
			Recapito r = null;
			switch (parts[i]) {
			case "T":
				r = new Telefono(parts[i+1], parts[i+2]);
				i += 3;
				break;
			case "E":
				r = new Email(parts[i+1], parts[i+2]);
				i += 3;
				break;
			case "I":
				r = new IndirizzoItaliano(parts[i+1], parts[i+2], 
						parts[i+3], parts[i+4], parts[i+5]);
				i += 6;
				break;
			case "S":
				r = new IndirizzoStraniero(parts[i+1], parts[i+2], 
						parts[i+3], parts[i+4]);
				i += 5;
				break;
			}
			if (r != null)
				recapiti.add(r);
		}
		if (recapiti.size() > 0)
			c.setRecapiti(recapiti.toArray(new Recapito[recapiti.size()]));
		return c;
	}
	
	public static class ContenutoFile {
		private Persona[] persone;
		private Azienda[] aziende;
		
		public ContenutoFile(Persona[] persone, Azienda[] aziende) {
			this.persone = persone;
			this.aziende = aziende;
		}

		public Persona[] getPersone() {
			return persone;
		}

		public Azienda[] getAziende() {
			return aziende;
		}
	}
	
	private static class NonInRubrica implements Recapito {

		@Override
		public String getDescrizione() {
			return "";
		}

		@Override
		public String getValore() {
			return "non in rubrica";
		}

		@Override
		public void utilizza() {}
		
	}
	
	public static void main(String[] args) {
		Persona[] per = new Persona[] {
				new Persona("p1", "cogn1", "nome1"),
				new Persona("p5", "cogn5", "nome5"),
				new Persona("p8", "cogn8", "nome8"),
				new Persona("p3", "cogn3", "nome3"),
		};
		Azienda[] az = new Azienda[] {
				new Azienda("az1", "Azienda 1"),
				new Azienda("az4", "Azienda 4"),
				new Azienda("az2", "Azienda 2"),
				new Azienda("az7", "Azienda 7"),
		};
		per[3].setLavoro(az[2]);
		per[1].setLavoro(az[3]);
		per[2].setLavoro(new Azienda("azX", "Azienda X"));
		per[3].setNascita(new GregorianCalendar(1954, 8, 14));
		
		per[3].setRecapiti(new Recapito[] {
				new Email("admin@provider.it"),
				new Telefono("Cellulare","3569875412"),
				new IndirizzoItaliano("via roma", "milano", "mi", "00100"),
				new Telefono("Casa", "1204569875")
		});
		az[3].setDirettore(per[1]);
		az[3].setRecapiti(new Recapito[] {
				new IndirizzoStraniero("Sede principale", "sonnestrasse", "monaco", "germania"),
				new Email("ufficio tecnico","support@azienda.de")
		});
		az[0].setDirettore(new Persona("perX", "cognox", "nox"));
		
		salva("test.txt", per, az);
		ContenutoFile cont = carica("test.txt");
		System.out.println(cont);
	}
}
