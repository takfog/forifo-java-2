package org.forifo.java2.rubrica.contatti;

public class Azienda extends Contatto {
	private String nome;
	private Persona direttore;
	
	public Azienda(String id, String nome) {
		super(id);
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Persona getDirettore() {
		return direttore;
	}

	public void setDirettore(Persona direttore) {
		this.direttore = direttore;
	}

	@Override
	public String toString() {
		return nome;
	}
}
