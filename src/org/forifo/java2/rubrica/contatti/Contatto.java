package org.forifo.java2.rubrica.contatti;

import org.forifo.java2.rubrica.recapiti.Recapito;

public class Contatto {
	private String id;
	private Recapito[] recapiti;
	
	public Contatto(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public Recapito[] getRecapiti() {
		return recapiti;
	}

	public void setRecapiti(Recapito[] recapiti) {
		this.recapiti = recapiti;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contatto other = (Contatto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
