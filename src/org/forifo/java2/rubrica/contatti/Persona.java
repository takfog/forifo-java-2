package org.forifo.java2.rubrica.contatti;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Persona extends Contatto {
	private String cognome;
	private String nome;
	private Calendar nascita;
	private Azienda lavoro;
	
	public Persona(String id, String cognome, String nome) {
		super(id);
		this.cognome = cognome;
		this.nome = nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public String getNome() {
		return nome;
	}

	public Calendar getNascita() {
		return nascita;
	}

	public void setNascita(Calendar nascita) {
		this.nascita = nascita;
	}

	public Azienda getLavoro() {
		return lavoro;
	}

	public void setLavoro(Azienda lavoro) {
		this.lavoro = lavoro;
	}
	
	@Override
	public String toString() {
		return cognome+" "+nome;
	}
	
	public static void main(String[] args) {
		Calendar inizioVacanze = new GregorianCalendar(2015, Calendar.DECEMBER, 23);
		System.out.println(inizioVacanze.getTimeInMillis());
		System.out.println(inizioVacanze.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY);
		System.out.printf("%1$td/%1$tm/%1$tY", inizioVacanze);
		
		Contatto p = new Persona("XYZ", "Rossi", "Mario");
		System.out.println(p);
	}
}
