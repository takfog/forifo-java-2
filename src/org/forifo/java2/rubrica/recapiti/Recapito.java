package org.forifo.java2.rubrica.recapiti;

public interface Recapito {
	public String getDescrizione();
	public String getValore();
	public void utilizza();
}
