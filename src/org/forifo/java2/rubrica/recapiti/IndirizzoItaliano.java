package org.forifo.java2.rubrica.recapiti;

public class IndirizzoItaliano extends Indirizzo {
	private String provincia;
	private String cap;
	
	public IndirizzoItaliano(String tipo, String via, String citta, String provincia, String cap) {
		super(tipo, via, citta);
		this.provincia = provincia;
		this.cap = cap;
	}
	
	public IndirizzoItaliano(String via, String citta, String provincia, String cap) {
		super(via, citta);
		this.provincia = provincia;
		this.cap = cap;
	}
	
	public String getProvincia() {
		return provincia;
	}

	public String getCap() {
		return cap;
	}

	@Override
	public String getValore() {
		return getVia()+" - "+cap+" "+getCitta()+" ("+provincia+")";
	}

	@Override
	public String getStato() {
		return "Italia";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cap == null) ? 0 : cap.hashCode());
		result = prime * result + ((provincia == null) ? 0 : provincia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		IndirizzoItaliano other = (IndirizzoItaliano) obj;
		if (cap == null) {
			if (other.cap != null)
				return false;
		} else if (!cap.equals(other.cap))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		return true;
	}
}
