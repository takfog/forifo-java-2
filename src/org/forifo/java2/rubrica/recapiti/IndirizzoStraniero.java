package org.forifo.java2.rubrica.recapiti;

public class IndirizzoStraniero extends Indirizzo {
	private String stato;
	
	public IndirizzoStraniero(String tipo, String via, String citta, String stato) {
		super(tipo, via, citta);
		this.stato = stato;
	}

	public IndirizzoStraniero(String via, String citta, String stato) {
		super(via, citta);
		this.stato = stato;
	}

	@Override
	public String getStato() {
		return stato;
	}

	@Override
	public String getValore() {
		return getVia()+" - "+getCitta()+", "+stato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((stato == null) ? 0 : stato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		IndirizzoStraniero other = (IndirizzoStraniero) obj;
		if (stato == null) {
			if (other.stato != null)
				return false;
		} else if (!stato.equals(other.stato))
			return false;
		return true;
	}
}
