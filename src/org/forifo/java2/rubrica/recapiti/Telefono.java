package org.forifo.java2.rubrica.recapiti;

public class Telefono implements Recapito {
	private String tipo;
	private String numero;
	
	public Telefono(String numero) {
		this("Telefono", numero);
	}
	
	public Telefono(String tipo, String numero) {
		this.tipo = tipo;
		this.numero = numero;
	}
	
	@Override
	public String getDescrizione() {
		return tipo;
	}
	
	@Override
	public String getValore() {
		return numero;
	}

	@Override
	public void utilizza() {
		System.out.println("Telefonata a "+numero);
	}
	
	@Override
	public String toString() {
		return tipo + ": "+numero;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefono other = (Telefono) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}
	
}
