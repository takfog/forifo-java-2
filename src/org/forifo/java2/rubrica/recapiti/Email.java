package org.forifo.java2.rubrica.recapiti;

import java.awt.Desktop;
import java.net.URI;

public class Email implements Recapito {
	private String tipo;
	private String indirizzo;
	
	public Email(String indirizzo) {
		this("Email", indirizzo);
	}
	
	public Email(String tipo, String indirizzo) {
		this.tipo = tipo;
		this.indirizzo = indirizzo;
	}
	
	@Override
	public String getDescrizione() {
		return tipo;
	}
	
	@Override
	public String getValore() {
		return indirizzo;
	}

	@Override
	public void utilizza() {
		if(Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().mail(new URI("mailto:"+indirizzo));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public String toString() {
		return tipo + ": "+indirizzo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indirizzo == null) ? 0 : indirizzo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Email other = (Email) obj;
		if (indirizzo == null) {
			if (other.indirizzo != null)
				return false;
		} else if (!indirizzo.equals(other.indirizzo))
			return false;
		return true;
	}
	
}
