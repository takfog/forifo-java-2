package org.forifo.java2.rubrica.recapiti;

import java.awt.Desktop;
import java.net.URI;

public abstract class Indirizzo implements Recapito {
	private String tipo;
	private String via;
	private String citta;
	
	public Indirizzo(String via, String citta) {
		this("Indirizzo", via, citta);
	}

	public Indirizzo(String tipo, String via, String citta) {
		this.tipo = tipo;
		this.via = via;
		this.citta = citta;
	}
	
	@Override
	public String getDescrizione() {
		return tipo;
	}
	
	@Override
	public void utilizza() {
		if(Desktop.isDesktopSupported()) {
			String indirizzo = getValore().replace(' ', '+');
			try {
				Desktop.getDesktop().browse(new URI("http://maps.google.it/maps?&q="+indirizzo));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getVia() {
		return via;
	}

	public String getCitta() {
		return citta;
	}
	
	public abstract String getStato();
	
	@Override
	public String toString() {
		return tipo+": "+getValore();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((citta == null) ? 0 : citta.hashCode());
		result = prime * result + ((via == null) ? 0 : via.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Indirizzo other = (Indirizzo) obj;
		if (citta == null) {
			if (other.citta != null)
				return false;
		} else if (!citta.equals(other.citta))
			return false;
		if (via == null) {
			if (other.via != null)
				return false;
		} else if (!via.equals(other.via))
			return false;
		return true;
	}
	
}
