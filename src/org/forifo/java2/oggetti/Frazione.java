package org.forifo.java2.oggetti;


public class Frazione {
	private int numeratore;
	private int denominatore;

	public Frazione(int numeratore, int denominatore) {
		this.numeratore = numeratore;
		this.denominatore = denominatore;
	}

	public int getNumeratore() {
		return numeratore;
	}

	public void setNumeratore(int numeratore) {
		this.numeratore = numeratore;
	}

	public int getDenominatore() {
		return denominatore;
	}

	public void setDenominatore(int denominatore) {
		this.denominatore = denominatore;
	}
	
	@Override
	public String toString() {
		return numeratore+"/"+denominatore;
	}

	@Override
	public int hashCode() {
		return Float.hashCode((float)numeratore/denominatore);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frazione other = (Frazione) obj;
		if (denominatore == other.denominatore && numeratore == other.numeratore)
			return true;
		if (denominatore*other.numeratore-numeratore*other.denominatore == 0)
			return true;
		else
			return false;
	}

	public static void main(String[] args) {
		Frazione f1 = new Frazione(3,6);
		Frazione f2 = new Frazione(1,2);
		Frazione f3 = new Frazione(1,5);
		System.out.println(f1);
		System.out.println(f2);
		System.out.println(f3);
		System.out.println(f1.equals(f2));
		System.out.println(f1.equals(f3));
		System.out.println(f1.hashCode());
		System.out.println(f2.hashCode());
		System.out.println(f3.hashCode());
	}
	
}