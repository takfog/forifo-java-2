package org.forifo.java2.oggetti;

public class Schermo {
	//campi
	private int dimensione;
	private int luminosita;
	private boolean acceso = false;

	//costruttore
	public Schermo(int dimensione) {
		this.dimensione = dimensione;
		luminosita = 5;
	}

	//getter di dimensione
	public int getDimensione() {
		return dimensione;
	}

	//getter della luminosita'
	public int getLuminosita() {
		return luminosita;
	}

	//setter della luminosita'
	public void setLuminosita(int luminosita) {
		if (luminosita >= 0 && luminosita <= 10) {
			this.luminosita = luminosita;
		}
	}

	//getter dello stato di accensione
	public boolean isAcceso() {
		return acceso;
	}

	public void accendi() {
		acceso = true;
	}

	public void spegni() {
		acceso = false;
	}

	public void mostra(String valore) {
		if (acceso) {
			System.out.println(valore);
		}
	}

	public void mostra(int numero) {
		mostra(numero, "default");
	}


	public void mostra(int numero, String valore) {
		if (acceso) {
			System.out.println("Numero "+numero+" "+valore);
		}
	}

	public static void main(String[] args) {
		Schermo s = new Schermo(15);
		Schermo s2 = new Schermo(18);
		s.mostra("sono spento");
		s.accendi();
		s.mostra("sono acceso");
		s2.mostra("e io invece?");
		System.out.println(s.isAcceso());
		s.spegni();
		s.mostra("sono di nuovo spento");
		System.out.println(s.isAcceso());
		System.out.println(s.getLuminosita());
		s.setLuminosita(11);
		System.out.println(s.getLuminosita());
		s.setLuminosita(8);
		System.out.println(s.getLuminosita());
		System.out.println(s2.getLuminosita());
		s.accendi();
		s.mostra(42);
		s.mostra("42");
	}
}





















