package org.forifo.java2.giocooca;

public class SaltaTurno implements Casella {

	@Override
	public void attiva(Giocatore g) {
		g.saltaProssimoTurno();
	}

	@Override
	public String toString() {
		return "X ";
	}
}
