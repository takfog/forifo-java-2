package org.forifo.java2.giocooca;


public class Giocatore {
	private String nome;
	private int posizione = 0;
	private boolean saltaTurno = false;
	
	public Giocatore(String nome){
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public int getPosizione() {
		return posizione;
	}
	
	public void setPosizione(int posizione) {
		this.posizione = posizione;
	}
	
	public void saltaProssimoTurno() {
		saltaTurno = true;
	}
	
	public int lanciaDado(Dado dado){
		if (saltaTurno){
			saltaTurno = false;
			return 0;
		} else {
			return dado.tira();
		}
	}
}
