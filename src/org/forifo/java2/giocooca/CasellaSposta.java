package org.forifo.java2.giocooca;

public class CasellaSposta implements Casella {
	public int passi;
	
	public CasellaSposta(int passi) {
		this.passi = passi;
	}
	
	@Override
	public void attiva(Giocatore g) {
		int pos = g.getPosizione() + passi;
		g.setPosizione(pos);
	}
	
	@Override
	public String toString() {
		if (passi > 0)
			return passi + " ";
		else
			return passi + "";
	}

}
