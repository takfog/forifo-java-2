package org.forifo.java2.giocooca;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Partita {
	private Dado dado = new Dado();
	private Tabellone tabellone = new Tabellone();
	private  ArrayList<Giocatore> giocatori;
	private int turno = 0;
		
	public Partita(File fileGiocatore) throws FileNotFoundException{
		Scanner s = new Scanner(fileGiocatore);
		giocatori = new ArrayList<>();
		while (s.hasNextLine()){
			String nome = s.nextLine();
			giocatori.add(new Giocatore(nome));
			
		}
		s.close();
		
	}
	public void stampa() {
		tabellone.stampa();
		for (Giocatore g : giocatori) {
			for(int i=0; i<g.getPosizione(); i++) {
				System.out.print("  ");
			}
			System.out.println(g.getNome());
		}
	}
	
	public static void main(String args[]){
		Partita p;
		try {
			p = new Partita(new File("giocatori.txt"));
		} catch (FileNotFoundException e) {
			System.err.println("Il file non esiste");
			return;
		}
		p.stampa();
	}
	
	public void gioca() {
		//todo per casa
	}

}
