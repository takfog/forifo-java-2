package org.forifo.java2.giocooca;

public class Tabellone {

	private Casella[] caselle;
	
	public Tabellone() {
		
		caselle = new Casella[20];
		caselle[5] = new CasellaSposta(2);
		caselle[8] = new CasellaSposta(-2);
		caselle[12] = new CasellaSposta(-2);
		caselle[11] = new SaltaTurno();
		caselle[15] = new CasellaSposta(2);
        caselle[18] = new SaltaTurno();

		for (int i = 0; i < caselle.length; i++) {
			if (caselle[i] == null)
				caselle[i] = new CasellaVuota();
		}
		
			
	}
	
	public boolean haVinto(Giocatore g) {
		return g.getPosizione() >= caselle.length;
	}
	
	public void attiva(Giocatore g) {
		caselle[g.getPosizione()].attiva(g);
	}
	
	public Casella[] getCaselle() {
		return caselle;
	}
	
	public void stampa() {
		for (Casella casella : caselle){
			System.out.print(casella.toString());
		}
		System.out.println();
	}
}
