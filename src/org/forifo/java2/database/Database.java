package org.forifo.java2.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {

	private Connection conn;

	public Database(String driver, String connStr) throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		conn = DriverManager.getConnection(connStr);
	}

	public ArrayList<Persona> getPersone() throws SQLException {
		Statement stmt = conn.createStatement();
		ResultSet res = stmt.executeQuery("SELECT id,nome,cognome FROM persone");
		return convertiRisultato(res);
	}

	private ArrayList<Persona> convertiRisultato(ResultSet res) throws SQLException {
		ArrayList<Persona> persona = new ArrayList<>();
		while (res.next()) {
			int id = res.getInt(1);
			String nome = res.getString(2);
			String cognome = res.getString(3);
			persona.add(new Persona(id, nome, cognome));
		}
		res.close();
		return persona;
	}

	public ArrayList<Persona> getPersone(int id) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement("SELECT id,nome,cognome FROM persone WHERE id=?");
		stmt.setInt(1, id);
		ResultSet res = stmt.executeQuery();
		return convertiRisultato(res);
	}

	public void close() throws SQLException {
		conn.close();
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Database db = new Database("org.h2.Driver", "jdbc:h2:./persone");
		for (Persona p : db.getPersone()) {
			System.out.println(p.getId() + " " + p.getNome() + " " + p.getCognome());
		}
		db.close();
	}
}
