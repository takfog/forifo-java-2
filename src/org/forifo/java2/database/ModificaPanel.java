package org.forifo.java2.database;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;

public class ModificaPanel extends JPanel {
	private JTextField txtCerca;
	private JLabel lblIdSelezionato;
	private JTextField txtNome;
	private JTextField txtCognome;

	/**
	 * Create the panel.
	 */
	public ModificaPanel() {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, 1.0};
		setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblId = new JLabel("ID");
		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.insets = new Insets(0, 0, 0, 5);
		gbc_lblId.anchor = GridBagConstraints.EAST;
		gbc_lblId.gridx = 0;
		gbc_lblId.gridy = 0;
		panel.add(lblId, gbc_lblId);
		
		txtCerca = new JTextField();
		GridBagConstraints gbc_txtCerca = new GridBagConstraints();
		gbc_txtCerca.insets = new Insets(0, 0, 0, 5);
		gbc_txtCerca.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCerca.gridx = 1;
		gbc_txtCerca.gridy = 0;
		panel.add(txtCerca, gbc_txtCerca);
		txtCerca.setColumns(10);
		
		JButton btnCerca = new JButton("Cerca");
		GridBagConstraints gbc_btnCerca = new GridBagConstraints();
		gbc_btnCerca.gridx = 2;
		gbc_btnCerca.gridy = 0;
		panel.add(btnCerca, gbc_btnCerca);
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 1.0};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblId_1 = new JLabel("ID");
		GridBagConstraints gbc_lblId_1 = new GridBagConstraints();
		gbc_lblId_1.anchor = GridBagConstraints.WEST;
		gbc_lblId_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblId_1.gridx = 0;
		gbc_lblId_1.gridy = 0;
		panel_1.add(lblId_1, gbc_lblId_1);
		
		lblIdSelezionato = new JLabel("");
		GridBagConstraints gbc_lblIdSelezionato = new GridBagConstraints();
		gbc_lblIdSelezionato.anchor = GridBagConstraints.WEST;
		gbc_lblIdSelezionato.insets = new Insets(0, 0, 5, 0);
		gbc_lblIdSelezionato.gridx = 1;
		gbc_lblIdSelezionato.gridy = 0;
		panel_1.add(lblIdSelezionato, gbc_lblIdSelezionato);
		
		JLabel lblNome = new JLabel("Nome");
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.WEST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 0;
		gbc_lblNome.gridy = 1;
		panel_1.add(lblNome, gbc_lblNome);
		
		txtNome = new JTextField();
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 1;
		panel_1.add(txtNome, gbc_txtNome);
		txtNome.setColumns(10);
		
		JLabel lblCognome = new JLabel("Cognome");
		GridBagConstraints gbc_lblCognome = new GridBagConstraints();
		gbc_lblCognome.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblCognome.insets = new Insets(0, 0, 0, 5);
		gbc_lblCognome.gridx = 0;
		gbc_lblCognome.gridy = 2;
		panel_1.add(lblCognome, gbc_lblCognome);
		
		txtCognome = new JTextField();
		GridBagConstraints gbc_txtCognome = new GridBagConstraints();
		gbc_txtCognome.anchor = GridBagConstraints.NORTH;
		gbc_txtCognome.insets = new Insets(0, 0, 0, 5);
		gbc_txtCognome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCognome.gridx = 1;
		gbc_txtCognome.gridy = 2;
		panel_1.add(txtCognome, gbc_txtCognome);
		txtCognome.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.NORTH;
		gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 2;
		add(panel_2, gbc_panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		JButton btnSvuota = new JButton("Svuota");
		panel_2.add(btnSvuota);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue);
		
		JButton btnInserisci = new JButton("Inserisci");
		panel_2.add(btnInserisci);
		
		JButton btnModifica = new JButton("Modifica");
		panel_2.add(btnModifica);
		
		JButton btnElimina = new JButton("Elimina");
		panel_2.add(btnElimina);

	}

}
