package org.forifo.java2.database;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class DbFrame extends JFrame {
	private Database db = null;
	
	private JTextField txtDriver;
	private JTextField txtConnStr;
	private JTable table;
	private JTextField txtCercaID;
	private JTextField txtCercaNome;
	private JTextField txtCercaCognome;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbTutti;
	private JRadioButton rdbID;
	private JRadioButton rdbNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DbFrame frame = new DbFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DbFrame() {
		setTitle("Base Dati");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 491, 385);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Connessione", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblDriver = new JLabel("Driver");
		GridBagConstraints gbc_lblDriver = new GridBagConstraints();
		gbc_lblDriver.anchor = GridBagConstraints.WEST;
		gbc_lblDriver.insets = new Insets(0, 0, 5, 5);
		gbc_lblDriver.gridx = 0;
		gbc_lblDriver.gridy = 0;
		panel.add(lblDriver, gbc_lblDriver);
		
		txtDriver = new JTextField();
		txtDriver.setText("org.h2.Driver");
		GridBagConstraints gbc_txtDriver = new GridBagConstraints();
		gbc_txtDriver.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDriver.insets = new Insets(0, 0, 5, 0);
		gbc_txtDriver.gridx = 1;
		gbc_txtDriver.gridy = 0;
		panel.add(txtDriver, gbc_txtDriver);
		txtDriver.setColumns(10);
		
		JLabel lblConnectionString = new JLabel("Connection string");
		GridBagConstraints gbc_lblConnectionString = new GridBagConstraints();
		gbc_lblConnectionString.anchor = GridBagConstraints.WEST;
		gbc_lblConnectionString.insets = new Insets(0, 0, 5, 5);
		gbc_lblConnectionString.gridx = 0;
		gbc_lblConnectionString.gridy = 1;
		panel.add(lblConnectionString, gbc_lblConnectionString);
		
		txtConnStr = new JTextField();
		txtConnStr.setText("jdbc:h2:./persone");
		GridBagConstraints gbc_txtConnStr = new GridBagConstraints();
		gbc_txtConnStr.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtConnStr.insets = new Insets(0, 0, 5, 0);
		gbc_txtConnStr.gridx = 1;
		gbc_txtConnStr.gridy = 1;
		panel.add(txtConnStr, gbc_txtConnStr);
		txtConnStr.setColumns(10);
		
		JButton btnNewButton = new JButton("Connetti");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connetti();
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.anchor = GridBagConstraints.EAST;
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 2;
		panel.add(btnNewButton, gbc_btnNewButton);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Ricerca", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.NORTH);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWeights = new double[]{0.0, 1.0};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panel_3.setLayout(gbl_panel_3);
		
		rdbID = new JRadioButton("ID");
		buttonGroup.add(rdbID);
		GridBagConstraints gbc_rdbID = new GridBagConstraints();
		gbc_rdbID.anchor = GridBagConstraints.WEST;
		gbc_rdbID.gridx = 0;
		gbc_rdbID.gridy = 1;
		panel_3.add(rdbID, gbc_rdbID);
		
		rdbTutti = new JRadioButton("Seleziona tutti");
		buttonGroup.add(rdbTutti);
		GridBagConstraints gbc_rdbTutti = new GridBagConstraints();
		gbc_rdbTutti.gridwidth = 2;
		gbc_rdbTutti.anchor = GridBagConstraints.WEST;
		gbc_rdbTutti.gridx = 0;
		gbc_rdbTutti.gridy = 0;
		panel_3.add(rdbTutti, gbc_rdbTutti);
		
		txtCercaID = new JTextField();
		GridBagConstraints gbc_txtCercaID = new GridBagConstraints();
		gbc_txtCercaID.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCercaID.gridx = 1;
		gbc_txtCercaID.gridy = 1;
		panel_3.add(txtCercaID, gbc_txtCercaID);
		txtCercaID.setColumns(10);
		
		rdbNome = new JRadioButton("Nome");
		buttonGroup.add(rdbNome);
		GridBagConstraints gbc_rdbNome = new GridBagConstraints();
		gbc_rdbNome.anchor = GridBagConstraints.WEST;
		gbc_rdbNome.gridx = 0;
		gbc_rdbNome.gridy = 2;
		panel_3.add(rdbNome, gbc_rdbNome);
		
		txtCercaNome = new JTextField();
		GridBagConstraints gbc_txtCercaNome = new GridBagConstraints();
		gbc_txtCercaNome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCercaNome.gridx = 1;
		gbc_txtCercaNome.gridy = 2;
		panel_3.add(txtCercaNome, gbc_txtCercaNome);
		txtCercaNome.setColumns(10);
		
		JLabel lblCognome = new JLabel("Cognome");
		GridBagConstraints gbc_lblCognome = new GridBagConstraints();
		gbc_lblCognome.anchor = GridBagConstraints.WEST;
		gbc_lblCognome.gridx = 0;
		gbc_lblCognome.gridy = 3;
		panel_3.add(lblCognome, gbc_lblCognome);
		
		txtCercaCognome = new JTextField();
		GridBagConstraints gbc_txtCercaCognome = new GridBagConstraints();
		gbc_txtCercaCognome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCercaCognome.gridx = 1;
		gbc_txtCercaCognome.gridy = 3;
		panel_3.add(txtCercaCognome, gbc_txtCercaCognome);
		txtCercaCognome.setColumns(10);
		
		JButton btnCerca = new JButton("Cerca");
		GridBagConstraints gbc_btnCerca = new GridBagConstraints();
		gbc_btnCerca.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnCerca.gridx = 1;
		gbc_btnCerca.gridy = 4;
		panel_3.add(btnCerca, gbc_btnCerca);
		btnCerca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cerca();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nome", "Cognome"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Modifica", null, panel_2, null);
	}
	
	private void connetti(){
		if (db != null) {
			try {
				db.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			db = null;
		}
		String driver = txtDriver.getText().trim();
		String connStr = txtConnStr.getText().trim();

		if (driver.isEmpty()) {
			warning("Inserisci il drive", "Campo mancante");
			return;
		}
		if (connStr.isEmpty()) {
			warning("Inserisci la stringa di connessione", "Campo mancante");
			return;
		}
		try {
			db = new Database(driver, connStr);
			JOptionPane.showMessageDialog(this, "Connessione effettuata");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			error("Drive non valido", "Errore");
		} catch (SQLException e) {
			e.printStackTrace();
			error("Errore durante la connessione", "Errore");
		}
	}
	
	private void cerca(){
		ArrayList<Persona> persone;
		try {
			if (rdbTutti.isSelected())
				persone = db.getPersone();
			else if(rdbID.isSelected())
				persone = db.getPersone(Integer.parseInt(txtCercaID.getText()));
			else
				persone= new ArrayList<>(0);		
		} catch (SQLException e) {
			e.printStackTrace();
			error("Errore durante la ricerca", "Errore");
			return;
		}
		
		Object[][] data = new Object[persone.size()][3];
		for (int i=0;i < persone.size(); i++){
			data[i][0]= persone.get(i).getId();
			data[i][1]= persone.get(i).getNome();
			data[i][2]= persone.get(i).getCognome();
		}
		table.setModel(new DefaultTableModel(data, new Object[]{"ID","Nome","Cognome"}));
	}
	
	private void warning(String messaggio, String titolo){
		JOptionPane.showMessageDialog(this, messaggio, titolo, JOptionPane.WARNING_MESSAGE);
	}
	
	private void error(String messaggio, String titolo){
		JOptionPane.showMessageDialog(this, messaggio, titolo, JOptionPane.ERROR_MESSAGE);
	}

}


