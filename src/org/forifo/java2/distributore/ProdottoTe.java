package org.forifo.java2.distributore;

public class ProdottoTe extends Prodotto {

	public ProdottoTe(int costo, int codice) {
		super(costo, "T�", codice);
	}

	@Override
	public Bevanda eroga() {
		return new Te();
	}

}
