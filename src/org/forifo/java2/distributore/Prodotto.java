package org.forifo.java2.distributore;

public abstract class Prodotto {
	private int costo;
	private String nome;
	private int codice;
	
	public Prodotto(int costo, String nome, int codice) {
		this.costo = costo;
		this.nome = nome;
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}
	
	public int getCosto() {
		return costo;
	}
	
	public int getCodice() {
		return codice;
	}
	
	public abstract Bevanda eroga();
	
	@Override
	public String toString() {
		return codice + " " + nome + " � " + (costo/100f); 
	}
}
