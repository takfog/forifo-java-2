package org.forifo.java2.distributore;

import java.util.ArrayList;

public class Distributore {
	private ArrayList<Prodotto> prodotti;
	
	public Distributore(ArrayList<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}

	public ArrayList<Prodotto> getProdotti() {
		return prodotti;
	}
	
	public void stampaProdotti() {
		for (Prodotto prodotto : prodotti) {
			System.out.println(prodotto);
		}
	}
	
	public Bevanda eroga(int codice) {
		for (Prodotto prodotto : prodotti) {
			if (prodotto.getCodice() == codice){
				return prodotto.eroga();
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		ArrayList<Prodotto> prodotti = new ArrayList<>();
		prodotti.add(new ProdottoCaffe(20, 73));
		prodotti.add(new ProdottoCioccolata(15, 42));
		prodotti.add(new ProdottoTe(25, 33));
		Distributore dist = new Distributore(prodotti);
		dist.stampaProdotti();
		
		dist.eroga(42).bevi();
	}
}
