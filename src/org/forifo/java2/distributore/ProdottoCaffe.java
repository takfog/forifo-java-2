package org.forifo.java2.distributore;

public class ProdottoCaffe extends Prodotto {
	
	public ProdottoCaffe(int costo, int codice) {
		super(costo, "Caff�", codice);
	}

	@Override
	public Bevanda eroga() {
		return new Caffe();
	}

}
