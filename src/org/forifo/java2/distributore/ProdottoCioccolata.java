package org.forifo.java2.distributore;

public class ProdottoCioccolata extends Prodotto {

	public ProdottoCioccolata(int costo, int codice) {
		super(costo, "Cioccolata", codice);
	}

	@Override
	public Bevanda eroga() {
		return new Cioccolata();
	}

}
