package org.forifo.java2.base;

public class Operatori {

	public static void main(String[] args) {
		
		//somma, sottrazione, prodotto
		int x = (1 - 2 + 3) * 4;
		System.out.println("x = "+x); // x = 8
		
		//divisione tra interi e resto della divisione
		int divisione = 5 / 3;
		int resto = 5 % 3;
		System.out.println("divisione = "+divisione); // divisione = 1
		System.out.println("resto = "+resto); // resto = 2
		
		//divisione in virgola mobile
		float a = 5;
		float b = 3;
		double divisoVirgola = a / b;
		System.out.println("divisoVirgola = "+divisoVirgola); // divisoVirgola = 1.6666666269302368
		
		//incremento
		int incremento = 5;
		incremento++; //equivale a incremento = incremento + 1
		System.out.println("incremento = "+incremento); // incremento = 6
		
		//decremento
		int decremento = 5;
		decremento--; //equivale a decremento = decremento - 1
		System.out.println("decremento = "+decremento); // decremento = 4
		
		//confronto di uguaglianza
		boolean uguale = 5 == 7;
		System.out.println("5 uguale a 7 ? "+uguale); // 5 uguale a 7 ? false
		
		//confronto di non uguaglianza
		boolean diverso = 5 != 7;
		System.out.println("5 diverso da 7 ? "+diverso); // 5 diverso da 7 ? true
		
		//and (entrambi veri)
		boolean andF = true && false;
		System.out.println("true && false = "+andF); // true && false = false
		boolean andV = true && true;
		System.out.println("true && true = "+andV); // true && true = true
		
		//or (almeno uno vero)
		boolean orV = false || true;
		System.out.println("false || true = "+orV); // false || true = true
		boolean orF = false || false;
		System.out.println("false || false = "+orF); // false || false = false
		
		//xor (uno vero e uno falso)
		boolean xorV = true ^ false;
		System.out.println("true ^ false = "+xorV); // true ^ false = true
		boolean xorF1 = true ^ true;
		System.out.println("true ^ true = "+xorF1); // true ^ true = false
		boolean xorF2 = false ^ false;
		System.out.println("false ^ false = "+xorF2); // false ^ false = false
	}
}
