package org.forifo.java2.base.esercizi;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Scrivere un programma che riceva tre numeri in virgola mobile 
 * come dati in ingresso, per poi stamparli in ordine crescente.
 */
public class Ordina3 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		float n1, n2, n3;
		
		System.out.print("Inserire primo numero: ");
		n1 = s.nextFloat();
		
		System.out.print("Inserire secondo numero: ");
		n2 = s.nextFloat();
		
		System.out.print("Inserire terzo numero: ");
		n3 = s.nextFloat();
		
		//metodo esplicito
		float o1, o2, o3;
		
		if(n1 < n2) {
			if(n1 < n3){
				o1 = n1; // n1 minore di tutti
				if(n2 < n3) {
					o2 = n2;
					o3 = n3;
				} else { // n2 >= n3
					o2 = n3;
					o3 = n2;
				}
			} else { // n1 >= n3 && n1 < n2
				o1 = n3;
				o2 = n1;
				o3 = n2;
			}
		} else { // n1 >= n2
			if(n1 < n3) {
				o1 = n2;
				o2 = n1;
				o3 = n3;
			} else { // n1 >= n3
				o3 = n1; // n1 maggiore di tutti
				if(n2 < n3) {
					o1 = n2;
					o2 = n3;
				} else { // n2 >= n3
					o1 = n3;
					o2 = n2;
				}
			}
		}
			
		System.out.println("Metodo esplicito: " + o1 + " <= " + o2 + " <= " + o3);
		
		//metodo "a bolle"
		float b1, b2, b3, tmp;
		b1 = n1;
		b2 = n2;
		b3 = n3;
		
		//mette in b1 il minore tra b1 e b2
		if (b2 < b1) {
			tmp = b1;
			b1 = b2;
			b2 = tmp;
		}
		//mette in b2 il minore tra b2 e b3
		if (b3 < b2) {
			tmp = b3;
			b3 = b2;
			b2 = tmp;
			//controlla se il nuovo b2 e' minore di b1
			if (b2 < b1) {
				tmp = b1;
				b1 = b2;
				b2 = tmp;
			}
		}
			
		System.out.println("Metodo a bolle: " + b1 + " <= " + b2 + " <= " + b3);

		//metodo breve
		//si puo' dichiarare un array indicando direttamente gli element che contiene
		float[] arr = new float[] { n1, n2, n3 };
		Arrays.sort(arr); //ordina un array qualunque
		System.out.println("Metodo breve: " + arr[0] + " <= " + arr[1] + " <= " + arr[2]);

	}
}
