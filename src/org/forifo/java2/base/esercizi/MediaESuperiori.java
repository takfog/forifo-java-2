package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Dati N interi scrivere un programma per trovare la 
 * media dei valori e contare i valori sopra la media.
 */
public class MediaESuperiori {

	public static void main(String[] args) {
		int[] valori;
		//crea uno scanner per leggere da tastiera
		Scanner s = new Scanner(System.in);
		
		System.out.print("Inserire il numero di valori: ");
		int n = s.nextInt(); //legge il primo numero come dimensione dell'array
		valori = new int[n]; //crea un array di dimensione n (indici da 0 a n-1)
		
		//per ogni elemento dell'array
		for(int i=0; i<valori.length; i++) {
			System.out.print("Inserire un valore: ");
			valori[i] = s.nextInt(); //memorizza il valore inserito da tastiera
		}
		
		int tot = 0;
		for (int v : valori) {
			tot += v; //equivale a tot = tot + v;
		}
		//per avere un risultato decimale dividendo tra due interi,
		//uno dei due valori deve essere trasformato in un decimale.
		//In questo caso viene trasformato tot tramite la formula  (float)tot
		float media = (float)tot/valori.length;
		
		//stampa il valore della media
		System.out.println("Media: "+media);
		
		int c = 0; //inizializza il contatore
		for (int v : valori) {
			if (v > media)	//se v e' maggiore della media
				c++;		//incrementa il contatore
		}
		
		//stampa il numero di valori maggiori della media
		System.out.println("Valori maggiori della media: "+c);
	}
}
