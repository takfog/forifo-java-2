package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Acquisire una stringa di caratteri, contenente pure degli spazi. 
 * Scrivere un programma tale che, ogni volta che viene trovato uno 
 * spazio, vengano soppressi eventuali altri spazi contigui (due o piu'). 
 * Ad esempio, se l'utente inserisce la stringa 
 * "Oggi e'     una      bella       giornata... di     pioggia!!!"
 * la stringa risultante sara' 
 * "Oggi e' una bella giornata... di pioggia!!!". 
 */
public class ComprimiSpazi {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String line = s.nextLine();
		//trasforma la stringa in un array di caratteri
		char[] a = line.toCharArray();
		
		System.out.print(a[0]); //stampa il primo carattere
		for(int i = 1; i < a.length; i++) {
			//se il carattere non e' uno spazio 
			//oppure il precedente non era uno spazio
			if( a[i] != ' ' || a[i-1] != ' ' ) {
				System.out.print(a[i]); 
			}
		}
	}
}
