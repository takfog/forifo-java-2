package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Dati N interi scrivere un programma 
 * per trovare la somma dei valori.
 */
public class SommaArray {

	public static void main(String[] args) {
		int[] valori;
		//crea uno scanner per leggere da tastiera
		Scanner s = new Scanner(System.in);
		
		System.out.print("Inserire il numero di valori: ");
		int n = s.nextInt(); //legge il primo numero come dimensione dell'array
		valori = new int[n]; //crea un array di dimensione n (indici da 0 a n-1)
		
		//per ogni elemento dell'array
		for(int i=0; i<valori.length; i++) {
			System.out.print("Inserire un valore: ");
			valori[i] = s.nextInt(); //memorizza il valore inserito da tastiera
		}
		
		int tot = 0;
		for (int v : valori) {
			tot += v; //equivale a tot = tot + v;
		}
		/* altro modo per scrivere questo ciclo
		for(int i=0; i<valori.length; i++) {
			int v = valori[i];
			tot += v;
		}
		*/
		
		//stampa il totale
		System.out.println("Totale: "+tot);
	}
}
