package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Scrivere un programma che, dato un certo tempo in giorni, ore, 
 * minuti e secondi, restituisca il numero totale di secondi. 
 */
public class TempoInSecondi {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int giorni, ore, minuti, secondi, totale;
		
		System.out.print("Inserire numero giorni: ");
		giorni = s.nextInt();
		
		System.out.print("Inserire numero ore: ");
		ore = s.nextInt();
		
		System.out.print("Inserire numero minuti: ");
		minuti = s.nextInt();
		
		System.out.print("Inserire numero secondi: ");
		secondi = s.nextInt();
		
		totale = secondi + minuti*60 + ore*60*60 + giorni*24*60*60;
		System.out.println("Il numero totale di secondi e' " + totale);
	}
}
