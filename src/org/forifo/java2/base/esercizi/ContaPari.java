package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Dati N interi scrivere un programma per contare i valori pari.
 */
public class ContaPari {

	public static void main(String[] args) {
		//crea uno scanner per leggere da tastiera
		Scanner s = new Scanner(System.in);
		
		System.out.print("Inserire il numero di valori: ");
		int n = s.nextInt(); //legge il numero di elementi che verranno inseriti
		
		int pari = 0; //inizializza il contatore
		//per ogni valore
		for(int i=0; i<n; i++) {
			System.out.print("Inserire un valore: ");
			int v = s.nextInt(); //legge il valore inserito da tastiera
			if ((v % 2) == 0) { //v e' pari se la divisione per 2 ha resto 0
				pari++; //v e' pari, incrementiamo il contatore
			}
		}
		
		//stampa il valore del contatore
		System.out.println("Sono stati inseriti "+pari+" numeri pari");
	}
}
