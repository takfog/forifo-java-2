package org.forifo.java2.base.esercizi;

import java.util.Scanner;

/**
 * Dati N interi scrivere un programma per trovare 
 * il valore minimo e massimo tra quelli inseriti.
 */
public class MinMax {

	public static void main(String[] args) {
		//crea uno scanner per leggere da tastiera
		Scanner s = new Scanner(System.in);
		
		System.out.print("Inserire il numero di valori: ");
		int n = s.nextInt(); //legge il numero di elementi che verranno inseriti
		
		//legge il primo valore per inizializzare il minimo e il massimo
		System.out.print("Inserire un valore: ");
		int v = s.nextInt(); //legge il valore inserito da tastiera
		int min = v;
		int max = v;
		
		//per ogni valore a partire dal secondo (il primo e' stato gia' letto)
		for(int i=1; i<n; i++) {
			System.out.print("Inserire un valore: ");
			v = s.nextInt(); //legge il valore inserito da tastiera
			
			if (v < min)	//se il nuovo valore e' minore del minimo
				min = v;	//aggiorna il minimo
			
			if (v > max)	//se il nuovo valore e' maggiore del massimo
				max = v;	//aggiorna il massimo
		}
		
		//stampa il minimo
		System.out.println("Valore minimo: "+min);
		//stampa il massimo
		System.out.println("Valore massimo: "+max);
	}
}
