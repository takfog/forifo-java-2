package org.forifo.java2.base;

public class Condizionali {

	public static void main(String[] args) {
		
		if (5 > 3) {
			System.out.println("5 maggiore di 3");
		} else {
			System.out.println("5 non maggiore di 3???");
		}
		
		if (5 <= 3) {
			System.out.println("5 minore di 3???");
		} else {
			System.out.println("5 maggiore o uguale a 3");
		}
		
		int a = 1;
		switch (a) {
		case 0:
			System.out.println("Caso zero");
			break;
		case 1:
			System.out.println("Caso uno");
			break;
		case 2: 
			System.out.println("Caso due");
			break;
		default:
			System.out.println("Altro caso");
			break;
		}
	}
}
