package org.forifo.java2.base;

public class HelloWorld {

	public static void main(String[] args) {
		//la doppia barra inizia un commento che viene ignorato dal compilatore
		
		//il classico hello world: stampa a schermo la scritta Ciao Mondo!
		System.out.println("Ciao Mondo!");
	}

}
