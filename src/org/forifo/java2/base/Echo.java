package org.forifo.java2.base;

import java.util.Scanner;

public class Echo {

	public static void main(String[] args) {
		//lo scanner ti permette di leggere un input
		//System.in contiene quello che viene scritto da tastiera sulla console
		Scanner s = new Scanner(System.in);
		//finche' c'e' una nuova linea da leggere
		while(s.hasNextLine()) {
			//leggi la linea scritta
			String line = s.nextLine();
			//stampa quello che e' stato scritto
			System.out.println("Hai scritto "+line);
		}
	}
}
